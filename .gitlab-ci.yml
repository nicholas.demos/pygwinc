stages:
  - dist
  - test
  - review
  - deploy

# have to specify this so that all jobs execute for all commits
# including merge requests
workflow:
  rules:
    - if: $CI_MERGE_REQUEST_ID
    - if: $CI_COMMIT_BRANCH

variables:
  GIT_STRATEGY: clone

# build the docker image we will use in all the jobs, with all
# dependencies pre-installed/configured.
gwinc/base:
  stage: dist
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE/$CI_JOB_NAME:$CI_COMMIT_REF_NAME
    GIT_STRATEGY: none
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - |
      cat <<EOF > Dockerfile
      FROM igwn/base:buster
      RUN apt-get update -qq
      RUN apt-get -y install --no-install-recommends git python3-gitlab python3 python3-yaml python3-scipy python3-matplotlib python3-lalsimulation python3-pypdf2 python3-h5py
      RUN git clone https://gitlab-ci-token:ci_token@git.ligo.org/gwinc/inspiral_range.git
      EOF
    - docker build -t $IMAGE_TAG .
    - docker push $IMAGE_TAG

# create plots for the canonical IFOs
generate_budgets:
  stage: test
  image: $CI_REGISTRY_IMAGE/gwinc/base:$CI_COMMIT_REF_NAME
  script:
      - mkdir -p ifo
      - export PYTHONPATH=/inspiral_range
      - for ifo in $(python3 -c "import gwinc; print(' '.join(gwinc.IFOS))"); do
      -     python3 -m gwinc $ifo -s ifo/$ifo.png -s ifo/$ifo.h5
      - done
      - python3 -m gwinc.ifo -s ifo/all_compare.png
  artifacts:
    when: always
    paths:
      - ifo

# this is a special job intended to run only for merge requests.
# budgets are compared against those from the target branch.  if the
# merge request has not yet been approved and noise changes are found,
# the job will fail.  once the merge request is approved the job can
# be re-run, at which point the pipeline should succeed allowing the
# merge to be merged.
noise_change_approval:
  stage: review
  rules:
    # - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "master"'
    - if: $CI_MERGE_REQUEST_ID
  image: $CI_REGISTRY_IMAGE/gwinc/base:$CI_COMMIT_REF_NAME
  script:
    - export PYTHONPATH=/inspiral_range
    - |
      cat <<EOF > merge_info_env.py
      import gitlab
      project_id = $CI_MERGE_REQUEST_PROJECT_ID
      mr_iid = $CI_MERGE_REQUEST_IID
      # this only works for public repos, otherwise need to specify
      # private_token=
      gl = gitlab.Gitlab('https://git.ligo.org')
      project = gl.projects.get(project_id)
      mr = project.mergerequests.get(mr_iid)
      approvals = mr.approvals.get()
      print('TARGET_URL={}'.format(project.http_url_to_repo))
      print('MR_APPROVED={}'.format(approvals.approved))
      EOF
    - python3 merge_info_env.py > ENV
    - . ENV
    - if [[ $MR_APPROVED != True ]] ; then
    -     echo "Approval not yet given, checking for noise changes..."
    -     git remote add upstream $TARGET_URL
    -     git remote update
    -     TARGET_REV=upstream/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME
    -     if ! python3 -m gwinc.test --git-rev $TARGET_REV -r gwinc_test_report.pdf ; then
    -         echo "NOISE CHANGES RELATIVE TO $TARGET_REV"
    -         echo "Approval required to merge this branch."
    -         /bin/false
    -     else
    -         echo "No noise changes detected, merge may proceed."
    -     fi
    - else
    -     echo "Merge request approved, noise change accepted."
    - fi
  artifacts:
    when: on_failure
    paths:
      - gwinc_test_report.pdf
    expose_as: 'PDF report of noise changes relative to target branch'

# generate the html doc web pages.  the "pages" job has special
# meaning, as it's "public" artifact becomes the directory served
# through gitlab static pages
pages:
  stage: deploy
  only:
    - master
  needs:
    - job: generate_budgets
      artifacts: true
  image: $CI_REGISTRY_IMAGE/gwinc/base:$CI_COMMIT_REF_NAME
  script:
    - rm -rf public
    - apt-get install -y -qq python3-sphinx-rtd-theme
    - cd docs
    - make html
    - cd ..
    - mv ./build/sphinx/html public
    - mv ifo public/
  artifacts:
    when: always
    paths:
      - public
